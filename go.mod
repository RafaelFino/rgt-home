module rgt-home

go 1.14

require (
	cloud.google.com/go v0.55.0 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
	google.golang.org/appengine v1.6.5
	google.golang.org/genproto v0.0.0-20200330113809-af700f360a68 // indirect
)
